#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *concat(char *src, char *dest, char *delimiter)
{
	char *concated = malloc(strlen(src) + strlen(dest) + 1);
	strcpy(concated, src);
	strcat(concated, delimiter);
	strcat(concated, dest);
	return concated;
}