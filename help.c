/**
 * ---------
 * help.c
 * ---------
 * displays linux shell help messages
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "concat.h"
#include "colors.h"
#define MAX_LINE 100000

void showHelp();
void showHelpProcess(char *process);

/**
 * main
 * @param argc
 * @param argv
 * @return 0
 */
int main(int argc, char *argv[])
{
    if (argc < 2) {
        showHelp();
    } else {
        showHelpProcess(argv[1]);
    }
    return 0;
}

/**
 * showHelp
 * shows generic help message of myShell
 */
void showHelp()
{
    char *args[3];
    args[0] = "/bin/less";
    args[1] = "help.txt";
    args[2] = NULL;
    if(execv("/bin/less", args)) {
        printf(ANSI_COLOR_RED "Error: help not found\n" ANSI_COLOR_RESET);
    }
    exit(0);
}

/**
 * showHelpProcess
 * shows help message of a specific process
 * @param process
 */
void showHelpProcess(char *process)
{
    char *result = malloc(strlen(process) + strlen(process) + 1);

    strcpy(result, process);
    strcat(result, "_help.txt");

    if (access(result, F_OK) == -1) {
        printf(ANSI_COLOR_RED "%s: No such application\n" ANSI_COLOR_RESET, process);
        exit(1);
    } else {
        char *args[3];
        args[0] = "/bin/less";
        args[1] = result;
        args[2] = NULL;

        if (execv(args[0], args))
        {
            printf(ANSI_COLOR_RED "Error: less not found" ANSI_COLOR_RESET);
        }
        exit(0);
    }
}
