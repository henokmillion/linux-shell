/**
 * ---------
 * copyFile.c
 * ---------
 * copies a file to another one by creating, appending to or
 *  replacing the content of the destination file
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "concat.h"
#include "colors.h"

#define MAX_LINE 100000

void copyFile(char *src, char *dest, int append);

/**
 * main
 * @param argc
 * @param argv
 * @return 1:0
 */
int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf(ANSI_COLOR_RED "Error: Incomplete input. Use help copyFile for help\n" ANSI_COLOR_RESET);
        return 1;
    } else {
        if (!strcmp("-a", argv[1])) {
            if (argc > 3) {
                copyFile(argv[2], argv[3], 1);
            } else {
                printf(ANSI_COLOR_RED "invalid input: use help copyFile for help\n" ANSI_COLOR_RESET);
            }
        } else {
            copyFile(argv[1], argv[2], 0);
        }
    }
    return 0;
}

/**
 * copyFile
 * @param src
 * @param dest
 * @param append
 */
void copyFile(char *src, char *dest, int append) {
    int srcFd;
    int destFd;
    char buff[MAX_LINE];
    srcFd = open(src, O_RDONLY);
    if (srcFd == -1) {
        printf(ANSI_COLOR_RED "%s: file does not exist" ANSI_COLOR_RESET, src);
        return;
    }
    if (append) {
        if (access(dest, F_OK) != -1) {
            destFd = open(dest, O_APPEND | O_CREAT | O_WRONLY);
        } else {
            printf(ANSI_COLOR_RED "%s: No such file or directory\n" ANSI_COLOR_RESET, dest);
            exit(1);
        }
    } else {
        if (fork() == 0) {
            if (access(dest, F_OK) != -1) {
                destFd = open(dest, O_WRONLY, S_IRWXU);
            } else {
                if (creat(dest, 0644) == -1) {
                    printf(ANSI_COLOR_RED "%s: file not created" ANSI_COLOR_RESET, dest);
                }
            }
        } else {
            wait(NULL);
        }
    }
    char *buffer = (char *) calloc(MAX_LINE, sizeof(char));
    ssize_t sz = read(srcFd, buffer, MAX_LINE);
    write(destFd, buffer, MAX_LINE);
    ssize_t sz2 = read(srcFd, buffer, MAX_LINE);

    close(srcFd);
    close(destFd);
    exit(0);
}