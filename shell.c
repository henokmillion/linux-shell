/**
 * -------------------
 * shell.c
 * -------------------
 * linux interactive shell
 * by: Henok Million, Nathan Getachew and Aman Bereket
 * AAIT, 2018, SE Group 6
 * Advisor: Mr. Eyob W.
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include "concat.h"
#include "colors.h"

#define MAX_LINE 100000

void run(char *argv[], int argc, int bg);
char *helpConcat(char *s1);
void processCommand(char *command, int *argc, char *argv[]);

/**
 * main
 */
void main() {
    char command[MAX_LINE / 2 + 1];
    int shouldrun = 1;
    int argc = 0;
    char *argv[MAX_LINE];
    while (shouldrun) {
        int bg = 0;
        printf(ANSI_COLOR_BLUE_BOLD "myShell> " ANSI_COLOR_RESET);
        fgets(command, MAX_LINE, stdin);
        processCommand(command, &argc, argv);
        if (!strcmp(command, "\n")) {
            continue;
        }
        if (feof(stdin)) {
            exit(0);
        }
        if (!strcmp("exit", argv[0])) {
            exit(0);
        }
        if (!strcmp("&", argv[argc - 1])) {
            bg = 1;
            argv[argc - 1] = NULL;
        }
        run(argv, argc, bg);
        shouldrun = 1;
    }
}

/**
 * run
 * @param argv
 * @param argc
 * @param bg
 */
void run(char *argv[], int argc, int bg) {
    char *custCommands[6] = {"createFile", "copyFile", "display", "createDir", "changeDir", "help"};
//    changeDir
    if (!strcmp(argv[0], custCommands[4])) {
        if (chdir(argv[1]) == 0) {
            printf(ANSI_COLOR_GREEN "Dir Changed to %s\n" ANSI_COLOR_RESET, argv[1]);
        } else {
            perror(argv[1]);
        }
        return;
    }
    if (fork() == 0) {
        // createFile
        if (!strcmp(argv[0], custCommands[0])) {
            if (argv[2] == NULL) {
                argv[2] = "";
            }
            char *args[3];
            args[0] = argv[0];
            args[1] = argv[1];
            args[2] = NULL;
            // execute createFile
            if (execv("createFile", args)) {
                printf("Error: file not created\n");
            }
            exit(0);
        }
//		copyFile
        if (!strcmp(argv[0], custCommands[1])) {
            if (strcmp(argv[1], "-a")) { //copyFile with "-a" flag
                char *args[4];
                args[0] = argv[0];
                args[1] = argv[1];
                args[2] = argv[2];
                args[3] = NULL;
                if (execv("copyFile", args)) {
                    printf(ANSI_COLOR_RED "Error: copyFile not found\n" ANSI_COLOR_RESET);
                }
                exit(0);
            } else {
                char *args[5];
                args[0] = argv[0];
                args[1] = argv[1];
                args[2] = argv[2];
                args[3] = argv[3];
                args[4] = NULL;
                if (execv("copyFile", args)) {
                    printf(ANSI_COLOR_RED "Error: copyFile not found\n" ANSI_COLOR_RESET);
                }
                exit(0);
            }
        }
//        display
        if (!strcmp(argv[0], custCommands[2])) {
            if (execv("display", argv)) {
                printf(ANSI_COLOR_RED "Error: display not found\n" ANSI_COLOR_RESET);
            }
            exit(0);
        }
//		createDir
        if (!strcmp(argv[0], custCommands[3])) {
            if (execv("createDir", argv)) {
                printf(ANSI_COLOR_RED "Error: createDir not found\n" ANSI_COLOR_RESET);
            }
        }
//        help
        if (!strcmp(argv[0], custCommands[5])) {
            if (argv[1] == NULL) {
                char *args[2];
                args[0] = "help";
                args[1] = NULL;
                if (execv("help", args)) {
                    printf(ANSI_COLOR_RED "Error: help not found\n" ANSI_COLOR_RESET);
                }
                return;
            } else {
                char *s1 = argv[1];
                char *args[3];
                args[0] = "help";
                args[1] = argv[1];
                args[2] = NULL;
                if (execv("help", args)) {
                    printf(ANSI_COLOR_RED "Error: help not found\n" ANSI_COLOR_RESET);
                }
                return;
            }
        }
        execvp(argv[0], argv);
        exit(0);
    } else {
        if (!bg) {
            wait(NULL);
        } else {
            return;
        }
    }
}

/**
 * processCommand
 * @param command
 * @param argc
 * @param argv
 */
void processCommand(char *command, int *argc, char *argv[]) {
    *argc = 0;
    int _argc = 0;
    char *token;
    command[strlen(command) - 1] = ' ';
    token = strtok(command, " ");
    while (token != NULL) {
        argv[_argc] = token;
        token = strtok(NULL, " ");
        _argc++;
    }
    argv[_argc] = NULL;
    *argc = _argc;
}