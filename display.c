/**
 * ---------
 * display.c
 * ---------
 * display the contents of a file to the standard output stream
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "concat.h"
#include "colors.h"
#define MAX_LINE 100000

void display(char *fileName);

/**
 * main
 * @param argc
 * @param argv
 * @return 1:0
 */
int main(int argc, char * argv[])
{
    if (argc < 2)
    {
        printf(ANSI_COLOR_RED "Error: filename not specified\n" ANSI_COLOR_RESET);
        return 1;
    } else
    {
        display(argv[1]);
    }
    return 0;
}

/**
 * display
 * @param file
 */
void display(char *fileName)
{
    unsigned long word;
    int fd = open(fileName, O_RDONLY);
    char *buffer = (char *) calloc(MAX_LINE, sizeof(char));
    if (fd == -1)
    {
        printf(ANSI_COLOR_RED "%s: no such file in directory" ANSI_COLOR_RESET, fileName);
    }
    else
    {
        ssize_t sz = read(fd, buffer, MAX_LINE);
        printf("%s\n", buffer);
    }
    close(fd);
}