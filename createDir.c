/**
 * ---------
 * createDir.c
 * ---------
 * create directory in $PWD or a specific path
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "colors.h"
#include "concat.h"

void createDir(char *dirName, char *path);

/**
 * main
 * @param argc
 * @param argv
 * @return int 1:0
 */
int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf(ANSI_COLOR_RED "Error: directory name not specified\n" ANSI_COLOR_RESET);
        return 1;
    } else {
        if (argc > 2) {
            createDir(argv[1], argv[2]);
        } else {
            createDir(argv[1], "./");
        }
    }
}

/**
 * createDir
 * @param dirName
 * @param path
 */
void createDir(char *dirName, char *path) {
    char *filePath = concat(path, dirName, "/");
    if (mkdir(filePath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != -1) { // create dir with linux standard permissions
        exit(0);
    }
    printf(ANSI_COLOR_RED "%s: directory already exists\n" ANSI_COLOR_RESET, dirName);
}