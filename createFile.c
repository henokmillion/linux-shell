/**
 * ---------
 * createFile.c
 * ---------
 * create a file in $PWD or a specific path
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "colors.h"
#include "concat.h"

#define MAX_LINE 100000
#define ERROR_EXIT 1
#define SUCCESS_EXIT 0

void createFile(char *fileName, char *path);

/**
 * main
 * @param argc
 * @param argv
 * @return ERROR_EXIT: SUCCESS_EXIT;
 */
int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf(ANSI_COLOR_RED "Error: filename not specified\n" ANSI_COLOR_RESET);
        return ERROR_EXIT;
    } else {
        if (argv[2] == NULL) {
            argv[2] = "./";
        }
        createFile(argv[1], argv[2]);
    }
    return SUCCESS_EXIT;
}

/**
 * createFile
 * @param fileName
 * @param path
 */
void createFile(char *fileName, char *path) {
    int fd;
    char *concated = concat(path, fileName, "/");
    if (access(fileName, F_OK) != -1) { //check to see if file exists
        printf(ANSI_COLOR_RED "%s: file already exists\n" ANSI_COLOR_RESET, fileName);
        exit(1);
    } else {
        if (open(concated, O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH) != -1) exit(0); //create new file in $PWD
        else printf(ANSI_COLOR_RED "%s: file not created" ANSI_COLOR_RESET, fileName);
    }
    close(fd);
}